# Calculadora Limite

<img src="https://img.shields.io/badge/version-1.2.0--2021-brightgreen">
<img src="https://img.shields.io/badge/language-C%2FC%2B%2B-blue">
<img src="https://img.shields.io/badge/platform-linux%20%7C%20macos-lightgrey">

## Proyecto 

### Introducción

Este proyecto está pensado para resolver limites que en el caso de ser indefinidos se resuelven con la regla del L'Hopital (SOLO DEL TIPO 0/0), para funciones compuestas por polinomios.

Nuestra motivación es resolver la problemática que tienen algunas calculadoras online, las cuales entregan un resultado `nulo` sin más, cuando el limite se indefine.

Para poder explicar el funcionamiento del código, necesitamos profundizar en cómo se lleva a cabo el cálculo de límites, derivadas y en que casos llamamos a la regla / teorema del L'Hopital:

En algunas ocaciones el calculo de algun punto en una funcion, hace que esta no tenga un valor definido un ejemplo de indeterminacion es 0/0, lo cual no es un numero definido y se le considera indeterminado, pero existe la posibilidad de llegar a darle un valor, aplicando la regla de L'Hopital, la cual consiste en que si es posible derivar tanto el numerador como el denominador, es decir, que existan factores del tipo x ^ n, con n mayor 0, es posible encontrar una solución a esta indeterminación.

La regla del L'Hopital puede ser utilizada solo en casos en que se llegue a 0/0 o Inf/Inf, pero en este proyecto solo se puede hacer calculos del tipo 0/0.

### Descripción del Proyecto

Primero que todo la calculadora está diseñada para ecuaciones del tipo `ax^n + ... + bx + c`, siendo tanto el numerador cómo el denominador de este mismo tipo.

En el caso de ser un límite 0/0, se aplicará la regla del L'Hopital, que consiste en derivar el polinomio del numerador y el denominador por separado denominador <sup>[1]</sup>. Este proceso se repite hasta obtener un valor no `Indeterminado`, sino se retornará el valor de la división de la derivada del numerador entre la derivada del numerador.

La calculadora podra hacer la cantidad de calculos que el usuario requiera y luego de cada calculo a través de QT se mostrara una solución gráfíca de la función ingresada y el límite representado como un punto de intersección definido en la función, donde la coordenada `x`, es el valor hacia donde tiende la función y la coordenada `y`, es el resultado del Límite. Esto sucederá siempre cuando el límite exista en la función, en el caso contrario, la salida gráfíca corresponderá a la representación de la función en el plano cartesiano.

### Funcionamiento del Programa

La aplicación tendra tres entradas de texto, una para el limite, otra para el numerador y una ultima para el denominador, para ingresar los datos deberas hacer click en la casilla de texto.

Para el caso del ingreso de datos en el numerador o el denominador, deberas hacer uso de la siguiente sintaxis: 
- Para escribir un monomio debes hacerlo de la siguiente manera: `ESCALARx^EXPONENTE`, ejemplo: `x^3`.
- En caso de que un escalar vaya sin `x^` se considerara el exponente como `0`.
- En caso de que un escalar vaya acompañado solo de `x` se considerara el exponente como `1`.
- Para unir monomios debes hacer uso de `+` o `-`, ejemplo: `x^3-8`.

Podras elegir que datos ingresar, ya que, por defecto ya vienen valores predeterminados, `x = 0`, `numerador = 1` y `denominador = 1`.

Luego de haber ingresado los datos, debes presionar el boton calcular, dando paso al backend de la app, esto hara que las casillas de texto, vuelvan a sus valores por defecto, pero los datos ingresados, estaran en la parte inferior izquierda. Ademas, se mostrara el punto limite y si es que se aplico la regla de L'Hopital para obtener dicho resultado.

**Constraints:** Los exponentes deben ser numeros naturales, es decir, enteros positivos. Tanto los escalares como el limite deben ser numeros enteros. No deben haber espacios en la escritura de la funcion.

### Requisitos del Proyecto

**Requisitos de compilación:** Para poder compilar este programa solo se necesita tener instalado `g++` ó `clang` en la sistema, ademas se necesitará tener instalada la libreria Open Source `Qt`.

**Requisitos de la plataforma:** Idealmente debería ser ejecutado desde cualquier `DISTRO LINUX`, pero es posible hacerlo desde cualquier otro `SO`, siempre y cuando cumpla los **requisitos de compilación**.

**Requisitos de resolución:** El dispositivo en el cual se ejecute esta app, debe tener como minimo una resolución de 1000x700, para que no se vea afectado su funcionamiento.

### Avances del Proyecto

Los avances están evidenciados en el [CHANGELOG](https://gitlab.com/zumodeus/calculadora-limite/-/blob/main/CHANGELOG.md).

### Diagrama de Componentes

<img src="https://gitlab.com/zumodeus/calculadora-limite/-/raw/main/assets/diagrama_de_componentes.png">

### Ejemplo de Funcionamiento

<img src="https://gitlab.com/zumodeus/calculadora-limite/-/raw/main/assets/test-image.png">

### Compilacion Y Ejecucion del Programa

```sh
# Entrar al directorio despues de clonar el repo
cd calculadora-limite

# Para compilar y crear el ejecutable ocupa este comando.
make graphic_calcusm

# Para ejecutar (LINUX)
./calcusm

# Para ejecutar (MACOS)
open calcusm.app
```

### Conclusión 

Logramos concretar todos los objetivos planteados en los tres hitos, siendo el mas importante de estos, el de implementar una salida grafica del problema planteado. Al incluir una nueva libreria al codigo se tuvieron que hacer muchos replanteamientos en el proyecto, lo cual llevo a la reestructuracion del mismo al punto de separar el codigo fuente funcional de lo otro, para poder obtener un mejor rendimiento durante su ejecucion y funcionamiento.

Finalmente, con la implentación de una entrada de datos, mediante la misma ventana, es decir, con una UI, permite hacer de la experiencia del usuario mas comoda. Calusm, aun no llega a su fin, puesto que aun podemos ampliar su alcance. A futuro tenemos pensado implementar el calculo de limites al infinito, ya sea negativo y positivo, para encontrar las asintotas horizontales de una función.

### Referencias

**Librerias:** [iostream](https://www.cplusplus.com/reference/iostream/), [vector](https://www.cplusplus.com/reference/vector/), [string](https://www.cplusplus.com/reference/string/), [cmath](https://www.cplusplus.com/reference/cmath/) y [Qt](https://doc.qt.io).
 
<sup>[1]</sup> Pag 104.-James Stewart | Cálculo en una Variable, CAPÍTULO 2 | LÍMITES Y DERIVADAS, 2010

**Fecha de entrega: `26 / 12 / 2021`**

## Creditos

```
Integrantes:
    - José Aros Aguirre | 202130556-4 | Programador | Task Tracker and Presentation Management.
    - Belén González Bravo | 202130552-1 | Programadora | Changelog and Readme Management.
    - Vicente Zúñiga Montenegro | 202130526-2 | Programador | Readme and Repo Git Management.

Ayudantes:
    - Matias Soto.
    - Cristobal Nilo.

Profesor:
    - Nicolás Galvez.
```

## Version

```sh
Calculadora Límite | Calcusm v1.2.0-2021
```
