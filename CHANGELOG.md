# CHANGELOG

## Hito 1 [Matematico-Algebraico] [V1.0.0-2021] [released]
### Añadidos
- Funcion input @zumodeus.
- Funciones de Derivacion @zumodeus.
- Funcion Calculo de Limite @josetomasaros2002.
- Funcion Ingreso de Ecuacion @Belen.Gonzalez.
- Funcionamiento del Programa @Belen.Gonzalez.

### Arreglos
- Bugeo en la entrada de datos del polinomio @zumodeus.

## Hito 2 [Visualizacion] [V1.1.0-2021] [released]
### Añadidos
- Calculo de Puntos @Belen.Gonzalez
- Visualizacion de la Grafica @zumodeus
- Función to_ecuation @josetomasaros2002

### Arreglos
- Reestructuración del Proyecto @josetomasaros2002 @Belen.Gonzalez @zumodeus
- Reescritura de Funciones @Belen.Gonzalez

## Hito 3 [Parametrizacion por interfaces] [V1.2.0-2021] [released]
### Añadidos
- Funcion Parser e Interpreter @Belen.Gonzalez.
- Funcion Get Data @josetomasaros2002.
- Funcion Set Data @zumodeus.
- Implementacion de entradas de texto @josetomasaros2002 @Belen.Gonzalez @zumodeus.

### Arreglos
- Arreglos en la interfaz grafica @josetomasaros2002 @Belen.Gonzalez @zumodeus.
- Bugeo en la entrada de datos de los polinomios y limite @zumodeus.
