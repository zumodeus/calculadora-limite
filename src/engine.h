#ifndef ENGINE_H
#define ENGINE_H

// Include
#include <iostream>
#include <vector>
#include <string>
#include <cmath>

// Definiciones
#define to_string std::to_string
#define getline std::getline
#define vector std::vector
#define string std::string
#define stoi std::stoi
#define stod std::stod
#define cin std::cin

// Factor
struct factor {
    int escalar = 1;
    int exponente = 0;
} default_factor;

// Limite
int limit = 0;

// Factor Vectors
vector<factor> n, d, numerador, denominador;

// Axis Points
vector<double> x_axis, y_axis;

// Critics Points
vector<double> critic_point;

// L'Hopital Aplicado
bool LHopital = false;

// Input
string input (string arg = "") {
    string response;
    printf("%s", arg.c_str());
    getline(cin, response);
    return response;
};

// To Stod
double to_stod (int value) {
    return stod(to_string(value) + ".01");
}

// Is Critic
bool is_critic (double x) {
    bool response = false;
    for (int i=0; i<critic_point.size(); i++) if (x == critic_point[i]) response = true;
    return response;
}

// Save Ecuacion
void save_ecuacion (vector<factor> & a, vector<factor> & b) {
    b.clear();
    for (int i=0; i<a.size(); i++)
        b.push_back(a[i]);
}

// Parser
factor parser (string x) {
    factor response;
    bool pass = false;
    string escalar, exponente;
    for (int i=0; i<x.length(); i++) {
        if (i == 0) escalar += x[i];
        else if (x[i] == 'x' || x[i] == '^') pass = true;
        else (pass ? exponente : escalar) += x[i];
    }
    response.escalar = stoi(escalar);
    response.exponente = (pass ? (exponente[0] ? stoi(exponente) : 1) : 0);
    return response;
}

// Interpreter
vector<factor> interpreter (string x) {
    string auxiliar;
    vector<factor> response;
    for (int i=0; i<x.length(); i++) {
        if (i == 0) {
            if ((x[0] == '-' && x[1] == 'x') || x[0] == 'x') auxiliar += (x[0] == '-' ? "-1" : "1");
            auxiliar += x[i];
        }
        else if (x[i] == '+' || x[i] == '-') {
            response.push_back(parser(auxiliar));
            auxiliar = x[i];
            if (x[i+1] == 'x') auxiliar += '1';
        }
        else auxiliar += x[i];
        if (i == x.length() - 1) response.push_back(parser(auxiliar));
    }
    return response;
}

// Derivar
void derivar (factor & monomio) {
    if (monomio.exponente > 0) {
        monomio.escalar *= monomio.exponente;
        monomio.exponente -= 1;
    } else {
        monomio.escalar = 0;
    }
}

// Derivar Ecuacion
void derivar_ecuacion (vector<factor> & polinomio) {
    for (int i=0; i<polinomio.size(); i++)
        derivar(polinomio[i]);
}

// Puede Derivarse
bool puede_derivarse (vector<factor> polinomio) {
    bool response = false;
    for (int i=0; i<polinomio.size(); i++) if (polinomio[i].escalar != 0 && polinomio[i].exponente != 0) response = true;
    return response;
}

// Calcular
double calcular (factor monomio, double limite) {
    return (double) monomio.escalar * pow(limite, monomio.exponente);
}

// Calcular Ecuacion
double calcular_ecuacion (vector<factor> polinomio, double limite) {
    double response = .0;
    for (int i=0; i<polinomio.size(); i++) response += std::round(calcular(polinomio[i], limite) * 1000.0) / 1000.0;
    return response;
}

// Calcular Limite
string calcular_limite (vector<factor> & a, vector<factor> & b, double limite) {

    // Locales
    int num, den;
    string resultado;

    // Running
    while (true) {

        // Setting
        num = calcular_ecuacion(a, limite), den = calcular_ecuacion(b, limite);

        // Checking
        if ((num == 0 && puede_derivarse(a)) && (den == 0 && puede_derivarse(b))) {
            derivar_ecuacion(a);
            derivar_ecuacion(b);
            LHopital = true;
        } else if (den == 0) {
            resultado = "NaN";
            break;
        } else {
            resultado = to_string((double) num / (double) den);
            break;
        }
    }

    // Return
    return resultado;
}

// Calcular Puntos
void calcular_puntos (double r_i, double r_d, vector<factor> num, vector<factor> den) {

    // Locals
    double a, b;

    // Running
    for (double x = r_i; x <= r_d; x += .01) {

        // Locals
        a = calcular_ecuacion(num, x);
        b = calcular_ecuacion(den, x);

        // Critics
        if (b == 0) critic_point.push_back(x);

        // Adding
        x_axis.push_back(x);
        y_axis.push_back(std::ceil((a / b) * 100.0) / 100.0);
    }
}

#endif
