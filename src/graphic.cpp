#include "graphic.h"
#include "ui_graphic.h"
#include "engine.h"

// Series Container
vector<QSplineSeries *> container;

// To Ecuation
QString to_ecuation (vector<factor> ecu) {

    // Locals
    string auxiliar = "";
    QString response = "";

    // Looping
    for (int i=0; i<ecu.size(); i++) {

        // Locals
        int esc = ecu[i].escalar, exp = ecu[i].exponente;

        // Checking
        if (esc != 0) {

            // Signo
            auxiliar += (i > 0 ? (esc > 0 ? " + " : " - ") : (esc > 0 ? "" : "- "));

            // Escalar
            esc = abs(esc);
            if (exp > 0)
                if (esc == 1 || esc == -1)
                    auxiliar += "";
                else
                    auxiliar += to_string(abs(esc));
            else
                auxiliar += to_string(abs(esc));

            // Factor
            if (exp > 0)
                auxiliar += string("x") + (exp > 1 ? string(" <sup>") + to_string(exp) + string("</sup>") : "");
        }

        // Setting
        response += QString::fromStdString(auxiliar);
        auxiliar = "";
    }

    // Return
    return response;
}

graphic::graphic(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::graphic)
{
    ui->setupUi(this);

    // Setting Defaults
    numerador.push_back(default_factor);
    denominador.push_back(default_factor);
    n.push_back(default_factor);
    d.push_back(default_factor);

    // Setting Default
    this->setData();

    // On Push Button
    connect(ui->d_button, SIGNAL(clicked()), this, SLOT(getData()));
}

void graphic::getData () {

    // Getting Limit
    limit = ui->c_input->text().toInt();

    // Getting Numerador
    numerador.clear(); n.clear();
    numerador = interpreter(ui->a_input->text().toStdString());
    save_ecuacion(numerador, n);

    // Getting Denominador
    denominador.clear(); d.clear();
    denominador = interpreter(ui->b_input->text().toStdString());
    save_ecuacion(denominador, d);

    // Resets
    ui->a_input->setText("1");
    ui->b_input->setText("1");
    ui->c_input->setText("0");
    x_axis.clear();
    y_axis.clear();
    LHopital = false;
    for (int i=0; i<container.size(); i++)
        delete container[i];
    container.clear();

    // Setters
    emit this->setData();
}

void graphic::setData() {

    // Setting
    ui->a_output->setText(to_ecuation(numerador));
    ui->b_output->setText(to_ecuation(denominador));
    ui->c_output->setText(QString::number(limit));

    // Creating Chart
    QChart * chart = new QChart;
    chart->legend()->hide();

    // Calculo de punto
    double limite = stod(calcular_limite(n, d, limit));
    ui->limit_coordinate->setText(isnan(limite) ? QString("Indeterminado") : QString("( ") + QString::number(limit) + QString(" , ") + QString::number(limite) + QString(" )"));
    ui->lhopital_bool->setText(QString(LHopital ? "" : "NO ") + QString("APLICADO"));

    // Calculo de puntos
    calcular_puntos(
        to_stod(limit - 10),
        to_stod(limit + 10),
        numerador,
        denominador
    );

    // Series Container
    QSplineSeries * series = new QSplineSeries();
    container.push_back(series);

    // Setting Points
    for (int i=0; i<x_axis.size(); i++) {
        if (isnan(y_axis[i]) || is_critic(x_axis[i])) {
            chart->addSeries(series);
            series = new QSplineSeries();
            container.push_back(series);
        } else
            series->append(x_axis[i], y_axis[i]);
    }

    // Setting Line Graphic
    chart->addSeries(series);

    // Creating and Setting Limit Point
    if (!isnan(limite)) {
        QScatterSeries * point = new QScatterSeries();
        point->setName("Limite");
        point->append(limit, limite);
        point->setMarkerShape(QScatterSeries::MarkerShapeCircle);
        point->setMarkerSize(10.);
        chart->addSeries(point);
    }

    // Setting Chart
    chart->createDefaultAxes();

    // Setting QChartView
    ui->graphic_view->setChart(chart);
}

graphic::~graphic()
{
    delete ui;
}
