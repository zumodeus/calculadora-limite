// Librerias
#include "engine.h"

// Input Ecuacion
void input_ecuacion (vector<factor> & polinomio) {
    factor aux;
    int cantidad = stoi(input("Ingrese la cantidad de elementos: "));
    if (cantidad >= 1) {
        for (int i=cantidad-1; i>=0; i--) {
            aux.exponente = i;
            aux.escalar = stoi(input("Ingrese el escalar del elemento x^" + to_string(i) + ": "));
            polinomio.push_back(aux);
        };
    } else {
        aux.exponente = 0; aux.escalar = 1; polinomio.push_back(aux);
    }
};

// Executable
int main () {

    // Option
    string option;

    // Menu
    printf("Bienvenido a Calcusm (la calculadora de los limites)\n");

    // Looping
    while (true) {

        // Setting
        numerador.clear(); denominador.clear();
        option = input("[Y/Yes to continue | N/No to stop]: ");

        // Checking
        if (option == "Y" || option == "Yes") {
            
            // Limite
            int limite = stoi(input("x -> "));

            // Setting
            printf("Ingrese los datos del numerador: \n");
            input_ecuacion(numerador);
            printf("Ingrese los datos del denominador: \n");
            input_ecuacion(denominador);

            // Calcular
            const char * value = calcular_limite(numerador, denominador, limite).c_str();

            // LHopital?
            if (LHopital) printf("L'Hopital Aplicado\n");

            printf("El resultado del limite es: %s\n", value);
        } else if (option == "N" || option == "No") {
            printf("Esperamos haberte ayudado, Adios.\n");
            break;
        } else {
            printf("Opcion no valida.\n");
        }
    };
    
    return 0;
};
