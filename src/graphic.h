#ifndef GRAPHIC_H
#define GRAPHIC_H

#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include <QSplineSeries>

QT_BEGIN_NAMESPACE
namespace Ui { class graphic; }
QT_END_NAMESPACE

class graphic : public QMainWindow
{
    Q_OBJECT

public:
    graphic(QWidget *parent = nullptr);
    ~graphic();

private slots:
    void getData();
    void setData();

private:
    Ui::graphic *ui;
};
#endif // GRAPHIC_H
