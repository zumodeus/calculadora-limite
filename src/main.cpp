#include "graphic.h"

#include <QApplication>

int main (int argc, char *argv[]) {
    QApplication a(argc, argv);
    graphic w;
    w.setFixedHeight(661);
    w.setFixedWidth(931);
    w.show();
    return a.exec();
}
